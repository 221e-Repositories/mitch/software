﻿/** 
 * @file Mitch_Data.cs
 * 
 * This file is part of Mitch_API library.

 * Mitch_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Mitch_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.If not, see<http://www.gnu.org/licenses/>.

 * Copyright (c) 2020 by 221e srl.
 */

using System;

namespace _221e.Mitch
{
    /// <summary>
    /// Class <c>Mitch_Data</c> defines a generic object to manage Mitch data.
    /// </summary>
    public class Mitch_Data
    {
        private ushort deviceID;
        /// <value>Property <c>DeviceID</c> represents the unique device identifier.</value>
        public ushort DeviceID
        {
            set { deviceID = value; }
            get { return deviceID; }
        }

        private UInt64 timestamp;
        /// <value>Property <c>Timestamp</c> represents the current date / time in epoch format.</value>
        public UInt64 Timestamp
        {
            set { timestamp = value; }
            get { return timestamp; }
        }

        private float[] gyroscope_ = new float[3];
        /// <value>Property <c>Gyr</c> represents the gyroscope reading around x, y, z-axis.</value>
        public float[] Gyr
        {
            get { return gyroscope_; }
            set
            {
                gyroscope_[0] = value[0];       // wx
                gyroscope_[1] = value[1];       // wy    
                gyroscope_[2] = value[2];       // wz
            }
        }

        private float[] accelerometer_ = new float[3];
        /// <value>Property <c>Axl</c> represents the accelerometer reading along x, y, z-axis.</value>
        public float[] Axl
        {
            get { return accelerometer_; }
            set
            {
                accelerometer_[0] = value[0];   // ax
                accelerometer_[1] = value[1];   // ay
                accelerometer_[2] = value[2];   // az
            }
        }

        private float[] magnetometer_ = new float[3];
        /// <value>Property <c>Mag</c> represents the magnetometer reading along x, y, z-axis.</value>
        public float[] Mag
        {
            get { return magnetometer_; }
            set
            {
                magnetometer_[0] = value[0];    // mx
                magnetometer_[1] = value[1];    // my
                magnetometer_[2] = value[2];    // mz
            }
        }

        private byte[] distance_ = new byte[2];
        /// <value>Property <c>TOF</c> represents the Time Of Flight reading for peripheral #1 and #2, respectively.</value>
        public byte[] TOF
        {
            get { return distance_; }
            set
            {
                distance_[0] = value[0];        // tof_#1
                distance_[1] = value[1];        // tof_#2
            }
        }

        private float[] pressure_ = new float[16];
        /// <value>Property <c>Pressure</c> represents the 16-channels insole pressure reading.</value>
        public float[] Pressure
        {
            set
            {
                pressure_[0] = value[0];
                pressure_[1] = value[1];
                pressure_[2] = value[2];
                pressure_[3] = value[3];
                pressure_[4] = value[4];
                pressure_[5] = value[5];
                pressure_[6] = value[6];
                pressure_[7] = value[7];
                pressure_[8] = value[8];
                pressure_[9] = value[9];
                pressure_[10] = value[10];
                pressure_[11] = value[11];
                pressure_[12] = value[12];
                pressure_[13] = value[13];
                pressure_[14] = value[14];
                pressure_[15] = value[15];
            }
            get { return pressure_; }
        }

        // Reserved for Future Use
        //private float[] quaternion_ = new float[4];
        //public float[] Quat
        //{
        //    get { return quaternion_; }
        //    set
        //    {
        //        quaternion_[0] = value[0];  // qw
        //        quaternion_[1] = value[1];  // qx
        //        quaternion_[2] = value[2];  // qy
        //        quaternion_[3] = value[3];  // qz
        //    }
        //}

        /// <summary>
        /// Report a Mitch data object in a string.
        /// </summary>
        /// <returns>Returns a string representing a Mitch object.</returns>
        public override string ToString()
        {
            string s =
                timestamp + "\t";
            for (int i = 0; i < 3; i++)
                s = s + this.gyroscope_[i] + "\t";
            for (int i = 0; i < 3; i++)
                s = s + this.accelerometer_[i] + "\t";
            for (int i = 0; i < 3; i++)
                s = s + this.magnetometer_[i] + "\t";
            for (int i = 0; i < 2; i++)
                s = s + this.distance_[i] + "\t";
            for (int i = 0; i < 15; i++)
                s = s + this.pressure_[i] + "\t";
            s = s + this.pressure_[15];
            return s;
        }
    }
}
