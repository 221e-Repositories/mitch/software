# Multipurpose InerTial CHamaleon

The package includes a complete set of routines, protocols, and tools for integrating the MITCH platform in your .Net projects.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

A detailed description of the communication protocol, as implemented in the present API is provided together with the source code.

## Prerequisites

.Net Standard 2.0.

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

Microsoft Visual Studio Community 2017 (VS15).

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [GitLab](https://about.gitlab.com/) to code, test, and deploy source code. It provides [Git](https://git-scm.com/) repository management.

## Authors

See the list of [contributors](https://gitlab.com/groups/221e-Repositories/mitch/-/group_members) who participated in this project.

## License

This project is licensed under the GNU General Public License - see the [LICENSE.md](LICENSE.md) file for details
