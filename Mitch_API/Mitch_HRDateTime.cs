﻿/** 
 * @file Mitch_HRDateTime.cs
 * 
 * This file is part of Mitch_API library.

 * Mitch_API is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Mitch_API is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Nome-Programma.If not, see<http://www.gnu.org/licenses/>.

 * Copyright (c) 2020 by 221e srl.
 */

using System;
using System.Runtime.InteropServices;

namespace _221e.Mitch
{
    /// <summary>
    /// Class <c>HRDateTime</c> defines high resolution date time object.
    /// </summary>
    public static class HRDateTime
    {
        /// <value>Property <c>IsAvailable</c> represents a boolean flag for track high resolution time availability on current platform.</value>
        public static bool IsAvailable { get; private set; }

        [DllImport("Kernel32.dll", CallingConvention = CallingConvention.Winapi)]
        private static extern void GetSystemTimePreciseAsFileTime(out long filetime);

        /// <summary>
        /// <c>UtcNow</c> represents a date time object in Universal Time Coordinated format.
        /// </summary>
        public static DateTime UtcNow
        {
            get
            {
                if (!IsAvailable)
                {
                    throw new InvalidOperationException(
                        "High resolution clock isn't available.");
                }

                long filetime;
                GetSystemTimePreciseAsFileTime(out filetime);

                return DateTime.FromFileTimeUtc(filetime);
            }
        }

        static HRDateTime()
        {
            try
            {
                long filetime;
                GetSystemTimePreciseAsFileTime(out filetime);
                IsAvailable = true;
            }
            catch (EntryPointNotFoundException)
            {
                // Not running Windows 8 or higher.
                IsAvailable = false;
            }
        }
    }
};